/*Hanna Salopaasi
+358405128662
hanna.salopaasi@gmail.com
*/


var htmlCanvas = document.getElementById("myCanvas");
var ctx = htmlCanvas.getContext("2d");
var elements = [];

initCanvas();

function initCanvas(){
	// Register an event listener to
	// call the resizeCanvas() function each time
	// the window is resized.
	window.addEventListener('resize', resizeCanvas, false);
	// Draw canvas border for the first time.
	resizeCanvas();
	// Add event listener for `click` events.
	htmlCanvas.addEventListener('click', function(event) {
	    var x = event.pageX,
	        y = event.pageY

	    // Collision detection between clicked offset and element.
	    elements.forEach(function(element) {
	        if (y > element.top && y < element.top + element.height 
	            && x > element.left && x < element.left + element.width) {
	        	var audio = new Audio(element.colour +'.mp3');
				audio.play();
	        }
	    });

	}, false);

}


// Display custom canvas.
// In this case it's a blue, 5 pixel border that
// resizes along with the browser window.
function redraw() {
	drawRectangles(window.innerWidth, window.innerHeight);
}

// Runs each time the DOM window resize event fires.
// Resets the canvas dimensions to match window,
// then draws the new borders accordingly.
function resizeCanvas() {
	htmlCanvas.width = window.innerWidth;
	htmlCanvas.height = window.innerHeight;
	redraw();
}


function drawRectangles(w, h){
	var wi= w/2;
	var he = h/2;
	//Add elements to elements array
	//Red
	elements.push({
    colour: 'red',
    width: wi,
    height: he,
    top: 0,
    left: 0
	})
	//Blue
	elements.push({
    colour: 'blue',
    width: wi,
    height: he,
    top: 0,
    left: wi
	})
	//Yellow
	elements.push({
    colour: 'yellow',
    width: wi,
    height: he,
    top: he,
    left: 0
	})
	//Green
	elements.push({
    colour: 'green',
    width: wi,
    height: he,
    top: he,
    left: wi
	})

	elements.forEach(function(element) {
    ctx.fillStyle = element.colour;
    ctx.fillRect(element.left, element.top, element.width, element.height);
})
}

